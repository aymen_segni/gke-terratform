# Variables declaration
variable "project_name" {
  type = "string"
}
variable "region" {
  type = "string"
}
variable "account" {
}

#Configure the Google Cloud provider

provider "google" {
  credentials = "${base64decode(var.account)}"
  project     = "${var.project_name}"
  region      = "${var.region}"
}
